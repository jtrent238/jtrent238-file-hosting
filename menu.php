<!DOCTYPE html
<html>
	<?php
		//include_once('login.php');
	?>
	<div class="w3-bar w3-blue" id="navbar">
		<a href="index.php" class="w3-bar-item w3-button w3-mobile">Home</a>
		<a href="https://github.com/jtrent238/jtrent238-file-hosting" class="w3-bar-item w3-button w3-mobile">Github</a>
		<a href="https://discord.gg/ndhwqfW" class="w3-bar-item w3-button w3-mobile">Discord</a>
		<a href="https://www.paypal.me/jtrent238" class="w3-bar-item w3-button w3-mobile">Donate</a>
		<a href="javascript:displayLogin('login')" class="w3-bar-item w3-button w3-mobile w3-right">Login</a>
		<a href="manage.php" class="w3-bar-item w3-button w3-mobile w3-right">Manage</a>
	</div>
</html>